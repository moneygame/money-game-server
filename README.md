Money Game V3
===

API
---
___

### Game Modules
___

#### All Game Modules
>Returns all game modules

Method: ` GET `

Route: `/api/v3/game_modules`

Response:
``` json
{
  game_modules: [
    id: number
    name: text
  ]
}
```
___

#### Specific Game Module
>Returns the required game module

Method: ` GET `

Route: `/api/v3/game_modules/:id`

Response:
``` json
{
  game_module: {
    id: number
    name: text
    sub_modules: [
        id: number
        game_module_id: number
        video_id: number
        name: text
    ]
  }
}
```
___

### Sub Modules
___

#### Sub Module
>Returns the required sub module

Method: ` GET `

Route: `/api/v3/sub_modules/:id`

Response:
``` json
{
  sub_module: [
    id: number
    game_module_id: number
    name: text
    video: {
        id: number
        url: text
        name: text
        questions: [
          {
            id: number
            body: text
            hint: text
            explanation: text
            question_options: [
              {
                id: number
                text: text
                correct: bool
              },
              ...
            ]
          }
        ]
    }
  ]
}
```
___

#### Player complete Sub Module
>Sets a submodule as completed for a player

Method: ` PUT `

Route: `/api/v3/sub_modules/:player_id/:sub_module_id/complete`

Response:

Success:
``` json
{
    completed_sub_module:
        {
            id: number
            sub_module_id: number
            player_id: number
        }
}
```

Error:
``` json
{
    errors: {...}
}
```


### Player
___

#### Get Player information
> Returns the player information

Method: `GET`

Route: `/api/v1/players/:id`

Response:
``` json
{
  player: {
    id: number,
    facebook_id: string,
    email: string,
    gender: string,
    birthdate: string,
    address: string,
    name: string,
    played_at: string,
    avatar_url: string,
    module_levels: [
      {
        id: number,
        game_module_id: number,
        level: number,
        level_manager_id: number,
        created_at: text,
        updated_at: text
      },
    ]
  }
}
```

___

#### Log in Player
> Updates the logged in player, if player doesn't exists, creates a new one

Method: `PUT`

Route: `api/v1/players/login`

Response:

Success:
``` json
{ 
  message: "Player logged in" 
}
```

Error:
``` json
{
  error: "An error occurred when trying to log in the player"
}
```
___

### Dictionary

___

#### List Dictionary Terms
> Returns all the saved terms and definitions

Method: `GET`

Route: `api/v1/dictionary_terms`

Response:
``` json
{
  dictionary_terms: [
    {
      term: string,
      definition: text
    },
    ...
  ]
}
```

___

### Community Questions

___

#### Post new Community Question
> Creates a new Community Question

Method: `POST`

Route: `api/v1/community_questions`

Params: 
``` json
{
  community_question: {
    player_id: number,
    question_id: number
  }
}
```
