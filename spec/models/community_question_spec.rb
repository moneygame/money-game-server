# == Schema Information
#
# Table name: community_questions
#
#  id             :integer          not null, primary key
#  question_id    :integer
#  player_id      :integer
#  created_at     :datetime
#  updated_at     :datetime
#  answered_at    :datetime
#  answered_by_id :integer
#

require 'rails_helper'

describe CommunityQuestion do
  describe "associations" do
    it {should belong_to(:question)}
    it {should belong_to(:player)}
  end

  describe "validations" do
    it {should validate_presence_of(:question)}
    it {should validate_presence_of(:player)}
  end

  describe "#closed_by" do
    subject { FactoryGirl.create :community_question }

    context "without a player_id" do
      it "returns false" do
        result = subject.closed_by(nil)
        expect(result).to be false
      end

      it "returns the blank message for answered_by" do
        subject.closed_by(nil)
        expect(subject.errors.full_messages).to include("Answered by can't be blank")
      end
    end

   context "with a player id" do
     it "returns true" do
       player = FactoryGirl.create(:player)

       result = subject.closed_by(player.id)
       expect(result).to be true
     end

     it "creates a new activity for the player who asked" do
       player = FactoryGirl.create(:player)
       expect(subject.player.activities).to receive(:create).with(name: "#{player.name} answered a question for #{subject.player.name}")
       subject.closed_by(player.id)
     end
   end
  end
end
