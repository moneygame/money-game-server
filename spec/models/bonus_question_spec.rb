# == Schema Information
#
# Table name: bonus_questions
#
#  id            :integer          not null, primary key
#  video_url     :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  sub_module_id :integer
#

require 'rails_helper'

describe BonusQuestion do
  describe "relations" do
    it { should have_many(:options).dependent(:destroy) }
  end

end
