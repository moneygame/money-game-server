# == Schema Information
#
# Table name: level_managers
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  player_id  :integer
#

require 'rails_helper'

describe LevelManager do
  describe '#associations' do
    it { is_expected.to belong_to(:player) }
    it { is_expected.to have_many(:module_levels) }
  end
end
