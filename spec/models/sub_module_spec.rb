# == Schema Information
#
# Table name: sub_modules
#
#  id               :integer          not null, primary key
#  game_module_id   :integer
#  name             :string(255)
#  order            :integer
#  opening_video_id :integer
#  closing_video_id :integer
#

require 'rails_helper'

describe SubModule do
  describe "#associations" do
    it { is_expected.to have_many(:questions) }
  end
end
