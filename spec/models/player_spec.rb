# == Schema Information
#
# Table name: players
#
#  id                           :integer          not null, primary key
#  email                        :string(255)
#  birthdate                    :date
#  gender                       :string(255)
#  name                         :string(255)
#  facebook_id                  :string(255)
#  address                      :string(255)
#  played_at                    :datetime
#  created_at                   :datetime
#  updated_at                   :datetime
#  avatar_url                   :string(255)
#  consecutive_played_days      :integer          default(0)
#  points                       :integer          default(0)
#  level                        :integer          default(1)
#  community_questions_answered :integer          default(0)
#  consecutive_levels           :integer          default(0)
#  device_name                  :string(255)
#  educational_mode             :boolean          default(FALSE)
#  completion_certificate_won   :boolean          default(FALSE)
#

require 'rails_helper'

describe Player do

  describe '#associations' do
    it { is_expected.to have_one(:level_manager) }
    it { is_expected.to have_many(:activities) }
    it { is_expected.to have_many(:player_login_activities) }
  end

  describe ".login" do
    subject {FactoryGirl.create(:player)}
    let!(:now) { Time.now }

    before do
      allow(Time).to receive(:now).and_return(now)
    end

    it "calls the update consecutive_played_days method" do
      expect(subject).to receive(:update_consecutive_played_days)
      subject.login
    end

    it "includes the new consecutive_played_days in the update attributes" do
      allow(subject).to receive(:consecutive_played_days).and_return(2)
      expect(subject).to receive(:update_attributes).with(hash_including(consecutive_played_days: 2))

      subject.login
    end

    it "updates the played at attribute" do
      expect(subject).to receive(:update_attributes).with(hash_including(played_at: now))
      subject.login
    end
  end

  describe '.level_up' do
    A_LEVEL = 3
    A_POINTS = 8
    CONSECUTIVE_LEVELS = 2

    subject { FactoryGirl.create :player }

    it "updates the player level" do
      subject.level_up(A_LEVEL, A_POINTS)
      expect(subject.level).to be A_LEVEL
    end

    it "updates the player score" do
      subject.level_up(A_LEVEL, A_POINTS)
      expect(subject.points).to be A_POINTS
    end

    it "creates a new activity for the player" do
      expect { subject.level_up(A_LEVEL, A_POINTS) }.to change { subject.activities.count }.by(1)
    end

    it "creates the activity with the correct message" do
      expect(subject.activities).to receive(:create).with(name: "Just passed Level #{A_LEVEL}")
      subject.level_up(A_LEVEL, A_POINTS)
    end

    it "updates the player consecutive levels" do
      subject.level_up(A_LEVEL, A_POINTS, CONSECUTIVE_LEVELS)
      expect(subject.consecutive_levels).to be CONSECUTIVE_LEVELS
    end
  end

  describe '.update_consecutive_played_days' do
    context "when played is nil" do
      let!(:player) {FactoryGirl.create :player}

      it "sets the consecutive_played_days to 1" do
        player.update_consecutive_played_days
        expect(player.consecutive_played_days).to be 1
      end
    end

    context "when played yesterday" do
      let!(:player) {FactoryGirl.create :player, played_at: Time.now - 1.day, consecutive_played_days: 2}

      it "increments the consecutive_played_days by 1" do
        expect { player.update_consecutive_played_days }.to change { player.consecutive_played_days }.by(1)
      end
    end

    context "when played 3 days ago" do
      let!(:player) {FactoryGirl.create :player, played_at: Time.now - 3.day, consecutive_played_days: 2}

      it "sets the consecutive_played_days to 1" do
        player.update_consecutive_played_days
        expect(player.consecutive_played_days).to be 1
      end
    end

    context "when played today" do
      let!(:player) {FactoryGirl.create :player, played_at: Time.now, consecutive_played_days: 2}

      it "keeps the same consecutive_played_days" do
        expect { player.update_consecutive_played_days }.not_to change {player.consecutive_played_days}
      end
    end
  end
end
