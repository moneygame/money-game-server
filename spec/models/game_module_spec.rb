# == Schema Information
#
# Table name: game_modules
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  order      :integer
#

require 'rails_helper'

describe GameModule do
  describe "#associations" do
    it { is_expected.to have_many(:module_levels) }
  end
end
