# == Schema Information
#
# Table name: questions
#
#  id                      :integer          not null, primary key
#  body                    :text
#  hint                    :text
#  created_at              :datetime
#  updated_at              :datetime
#  explanation             :text
#  image_hint_file_name    :string(255)
#  image_hint_content_type :string(255)
#  image_hint_file_size    :integer
#  image_hint_updated_at   :datetime
#  hint_type               :string(255)
#  times_failed            :integer          default(0)
#  times_gained            :integer          default(0)
#  sub_module_id           :integer
#

require 'rails_helper'

describe Question do
  describe "relations" do
    it { should belong_to(:sub_module) }
    it { should have_many(:options).dependent(:destroy) }
    it { should have_attached_file(:image_hint) }

    it "user chooses an image for the image hint" do
      should validate_attachment_content_type(:image_hint).
        allowing(%w{ image/png image/gif image/jpg}).
        rejecting(%w{ text/plain application/pdf})
    end
  end
end
