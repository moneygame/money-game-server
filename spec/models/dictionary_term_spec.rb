# == Schema Information
#
# Table name: dictionary_terms
#
#  id         :integer          not null, primary key
#  term       :string(255)
#  definition :text
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

describe DictionaryTerm, "validations" do
  it { should validate_presence_of(:term) }
  it { should validate_presence_of(:definition) }
end
