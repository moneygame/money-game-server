require 'rails_helper'
require 'money_game/badges_verifier/login_badges'

describe MoneyGame::BadgesVerifier::LoginBadges, "#verify_badges_for" do
  let!(:activities) { double(:activities, create: true) }
  let!(:badges) { double(:badges, create: true) }
  let!(:player) { double(:player, badges: badges, activities: activities) }

  it_should_behave_like "a badges verifier", :consecutive_played_days, {
    2 => "Back-to-back learner",
    5 => "Non-stop player"
  }

  context "when the player played 1 consecutive days" do
    before do
      allow(player).to receive(:consecutive_played_days).and_return(1)
    end

    it "doesn't add a badge to the player" do
      expect(player.badges).not_to receive(:add_badge)
      described_class.verify_badges_for(player)
    end
  end
end
