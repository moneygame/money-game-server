shared_examples "a badges verifier" do |attr, rules|
  rules.each do |value, badge_name|
    context "when the player played #{value} #{attr}" do
      before do
        allow(player).to receive(attr).and_return(value)
      end

      it "creates a '#{badge_name}' badge for the user" do
        expect(player.badges).to receive(:create).with(badge_name: badge_name)
        described_class.verify_badges_for(player)
      end

      it "creates an award activity for '#{badge_name}' badge" do
        expect(player.activities).to receive(:create).with(name: "Award #{badge_name} badge")
        described_class.verify_badges_for(player)
      end
    end
  end
end
