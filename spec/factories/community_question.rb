FactoryGirl.define do

  factory :option do
    text "option"
    correct false
    answerable_id nil
    answerable_type nil
  end

  factory :question do
    ignore do
      options_count 4
    end

    body "Question Body"
    hint "Question hint"
    explanation "Questions Explanation"

    before(:create) do |question, evaluator|
      question.options = [FactoryGirl.create(:option, answerable_id: question.id, answerable_type: "Question", correct: true)]
      question.options << [FactoryGirl.create(:option, answerable_id: question.id, answerable_type: "Question")] * (evaluator.options_count - 1)
    end
  end

  factory :community_question do
    player
    question
  end
end
