FactoryGirl.define do
  factory :player do
    sequence(:email)  {|n| "player_#{n}@moneygame.com"}
    birthdate          { 15.years.ago }
    gender            "Male"
    name              "Player"
    facebook_id       { "fb_#{Random.rand(1000...9999)}" }
    address           "620 davis st"
    played_at         nil
    consecutive_played_days 0
    consecutive_levels 0
    community_questions_answered 0
  end
end
