require 'rails_helper'

describe API::V1::CommunityQuestionsController, "#create" do
  context "with the right params" do
    it "retuns a 200 status" do
      allow_any_instance_of(CommunityQuestion).to receive(:save).and_return(true)
      post :create, community_question: {player_id: 1, question_id: 100}
      expect(response).to have_http_status(:ok)
    end
  end

  context "with invalid params" do
    it "returns a 422 status" do
      allow_any_instance_of(CommunityQuestion).to receive(:save).and_return(false)
      post :create, community_question: {player_id: 1, question_id: 100}
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end

describe API::V1::CommunityQuestionsController, "#close" do
  A_PLAYER_ID = 1

  let!(:community_question) { FactoryGirl.create :community_question }
  let(:player) { double :player }

  before do
    allow(CommunityQuestion).to receive(:find).and_return(community_question)
    allow(Player).to receive(:find).with(A_PLAYER_ID).and_return(player)
    allow(MoneyGame::BadgesVerifier::CommunityBadges).to receive(:verify_badges_for).with(player)
  end

  it "calls closed_by method with the answered_by param" do
    expect(community_question).to receive(:closed_by).with(A_PLAYER_ID)
    put :close, community_question_id: community_question.id, answered_by: A_PLAYER_ID
  end

  context "when the question is closed" do
    before do
      allow(community_question).to receive(:closed_by).and_return(true)
    end

    it "returns a 200 status" do
      put :close, community_question_id: community_question.id, answered_by: A_PLAYER_ID
      expect(response).to have_http_status(:ok)
    end

    it "checks for community_badges" do
      expect(MoneyGame::BadgesVerifier::CommunityBadges).to receive(:verify_badges_for).with(player)
      put :close, community_question_id: community_question.id, answered_by: A_PLAYER_ID
    end
  end

  context "when the question failed to close" do
    it "returns a unprocessable_entity status" do
      allow(community_question).to receive(:closed_by).and_return(false)
      put :close, community_question_id: community_question.id, answered_by: A_PLAYER_ID
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
