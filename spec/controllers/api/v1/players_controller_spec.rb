require 'rails_helper'

describe API::V1::PlayersController, "#show" do
  context "for an existing player" do
    it "returns a ok status" do
      player = FactoryGirl.create :player
      get :show, id: player.id
      expect(response).to have_http_status(:ok)
    end
  end
  context "for an unexisting player" do
    it "returns nil" do
      get :show, id: 999
      expect(response.body).to include "null"
    end
  end
end

describe API::V1::PlayersController, "#login" do
  let!(:player_params) { {player: FactoryGirl.attributes_for(:player)} }
  let!(:wrong_player_params) { { player: FactoryGirl.attributes_for(:player, facebook_id: "") } }

  context "on success update" do
    it "returns a success message" do
      post :login, player_params
      player = Player.find_by(facebook_id: player_params[:player][:facebook_id])
      expect(response.body).to include PlayerSerializer.new(player).to_json
    end

    it "returns an ok status response" do
      post :login, player_params
      expect(response).to have_http_status(:ok)
    end

    it "checks for login badges" do
      expect(MoneyGame::BadgesVerifier::LoginBadges).to receive(:verify_badges_for).with(an_instance_of(Player))
      post :login, player_params
    end

    context "when the player doesn't exists" do
      it "creates a new player" do
        expect {post :login, player_params }.to change {Player.count}.by(1)
      end
    end

    context "when a player with the facebook_id exists" do
      before { FactoryGirl.create :player, facebook_id: "FB_1" }

      it "doesn't create new player" do
        player_params = {player: {facebook_id: "FB_1"}}
        expect {post :login, player_params }.not_to change {Player.count}
      end
    end

  end

  context "when update fails" do

    before { allow_any_instance_of(Player).to receive(:login).and_return(nil) }

    it "returns a error message" do
      post :login, wrong_player_params
      expect(response.body).to include "An error occurred when trying to log in the player"
    end

    it "responds with an unprocessable_entity status" do
      post :login, wrong_player_params
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end

describe API::V1::PlayersController, "#level_up" do
  A_PLAYER_ID = 4
  A_LEVEL = 3
  A_POINTS = 200
  A_CONSECUTIVE_LEVEL=1
  let!(:player) { FactoryGirl.create :player }

  before :each do
    allow(Player).to receive(:find).with(A_PLAYER_ID.to_s).and_return(player)
  end

  it "looks for the player with the player_id" do
    expect(Player).to receive(:find).with(A_PLAYER_ID.to_s).and_return(player)
    put :level_up, {player_id: A_PLAYER_ID, player: {level: A_LEVEL, points: A_POINTS}}
  end

  it "calls player's level up" do
    expect(player).to receive(:level_up).with(A_LEVEL.to_s, A_POINTS.to_s, A_CONSECUTIVE_LEVEL.to_s).and_return(true)
    put :level_up, {player_id: A_PLAYER_ID, player: {level: A_LEVEL, points: A_POINTS, consecutive_levels: A_CONSECUTIVE_LEVEL}}
  end

  it "checks for level badges" do
    expect(MoneyGame::BadgesVerifier::LevelBadges).to receive(:verify_badges_for).with(player)
    put :level_up, {player_id: A_PLAYER_ID, player: {level: A_LEVEL, points: A_POINTS}}
  end

  context "when level up returns true" do
    it "responds with the serialized player" do
      put :level_up, {player_id: A_PLAYER_ID, player: {level: A_LEVEL, points: A_POINTS}}
      expect(response.body).to eq PlayerSerializer.new(player).to_json
    end

    it "responds with a ok status" do
      put :level_up, {player_id: A_PLAYER_ID, player: {level: A_LEVEL, points: A_POINTS}}
      expect(response).to have_http_status(:ok)
    end
  end

  context "when level up returns false" do
    it "responds with a unprocessable_entity status" do
      allow(player).to receive(:level_up).and_return(false)

      put :level_up, {player_id: A_PLAYER_ID, player: {level: A_LEVEL, points: A_POINTS}}
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
