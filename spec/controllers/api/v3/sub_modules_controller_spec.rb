require 'rails_helper'

describe API::V3::SubModulesController, "#index" do
  it "returns all submodules" do
    mod = FactoryGirl.create :game_module
    Random.rand(20).times do
      FactoryGirl.create :sub_module, game_module_id: mod.id
    end
    get :index
    expect(JSON.parse(response.body)["sub_modules"].count).to eq(SubModule.count)
  end
end

describe API::V3::SubModulesController, "#show" do
  before do
    @module = FactoryGirl.create :game_module
  end

  context "when submodule exists" do
    it "returns the requested submodule" do
      sub_module = FactoryGirl.create :sub_module, game_module_id: @module.id
      get :show, id: sub_module.id
      expect(JSON.parse(response.body)["name"]).to eq(sub_module.name)
    end
  end

  context "when submodule does not exist" do
    it "returns nil" do
      get :show, id: 999
      expect(response.body).to eq("null")
    end
  end
end

describe API::V3::SubModulesController, "#questions" do
  before do
    @module = FactoryGirl.create :game_module
  end

  context "when submodule exists" do
    it "returns the requested submodule's questions" do
      sub_module = FactoryGirl.create :sub_module, game_module_id: @module.id
      question_count = 5
      question_count.times do
        FactoryGirl.create :question, sub_module_id: sub_module.id
      end
      get :questions, sub_module_id: sub_module.id
      expect(JSON.parse(response.body)['questions'].count).to eq(question_count)
    end
  end

  context "when submodule does not exist" do
    it "returns error" do
      get :questions, sub_module_id: 999
      expect(response.body).to include("Sub module not found")
    end
  end
end

describe API::V3::SubModulesController, "#complete" do
  before do
    @module = FactoryGirl.create :game_module
    @sub_module = FactoryGirl.create :sub_module, game_module_id: @module.id
  end

  it "creates a completed sub module" do
    player = Player.first
    expect{put :complete, sub_module_id: @sub_module.id, player_id: player.id}.to change{CompletedSubModule.count}.by(1)
    completed_sub_module = CompletedSubModule.last
    expect(completed_sub_module.sub_module_id).to eq(@sub_module.id)
    expect(completed_sub_module.player_id).to eq(player.id)
  end
end
