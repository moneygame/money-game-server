require 'simplecov'

SimpleCov.start 'rails' do
  add_filter("/app\/admin/")
end

RSpec.configure do |config|
end
