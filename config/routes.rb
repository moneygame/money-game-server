Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root "admin/dashboard#index"

  get 'module_questions_download/:game_module_id', to: "page#module_questions_download", as: "module_questions_download"

  namespace :api do
    namespace :v1 do
      resources :game_modules, only: [:index, :show]

      resources :level, only: [], param: :number do
        resources :questions, only: :index
        resource :bonus_question, only: :show
      end

      resources :questions, only: :show do 
        get :max_level, on: :collection
        get :ninety_percent_questions, on: :collection
        put :update_gained_question_counter
        put :update_failed_question_counter
      end

      resources :players, only: [:show] do
        put :login, on: :collection
        put :level_up
        put :playing_educational_mode
        put :completion_certificate_won 
        put :dictionary_views
        put :coaching_page_views
        put :calculator_views
        put :completed_modules
        put :level_activities
        get :activities, to: 'activities#for_player'
      end

      resources :dictionary_terms, only: :index

      resources :community_questions, only: [:index, :create] do
        put :close
      end

      resources :activities, only: [:index] do
        get :likes
        put :likes_up
      end

      resources :videos, only: :index

    end
  end

  ## New version of the api to use in the mobile application of MG3
  namespace :api do
    namespace :v2 do
      resources :game_modules, only: [:index, :show]
      get '/game_modules/:id/:module_level/questions', to: 'game_modules#questions'
      put '/module_levels/:player_id/:game_module_id/level_up', to: 'module_levels#level_up'

      resources :level, only: [], param: :number do
        resources :questions, only: :index
        resource :bonus_question, only: :show
      end

      resources :questions, only: :show do 
        get :max_level, on: :collection
        get :ninety_percent_questions, on: :collection
        put :update_gained_question_counter
        put :update_failed_question_counter
      end

      resources :players, only: [:show] do
        put :login, on: :collection
        put :level_up
        put :playing_educational_mode
        put :completion_certificate_won 
        put :dictionary_views
        put :coaching_page_views
        put :calculator_views
        put :completed_modules
        put :level_activities
        get :activities, to: 'activities#for_player'
      end

      resources :dictionary_terms, only: :index

      resources :community_questions, only: [:index, :create] do
        put :close
      end

      resources :activities, only: [:index] do
        get :likes
        put :likes_up
      end

      resources :videos, only: :index

    end
  end

  namespace :api do
    namespace :v3 do
      resources :game_modules, only: [:index, :show]

      resources :sub_modules, only: [:index, :show] do
        get :questions
      end

      put '/sub_modules/:player_id/:sub_module_id/complete', to: 'sub_modules#complete'

      resources :questions, only: :show do
        get :max_level, on: :collection
        get :ninety_percent_questions, on: :collection
        put :update_gained_question_counter
        put :update_failed_question_counter
      end

      resources :players, only: [:show] do
        put :login, on: :collection
        put :level_up
        put :playing_educational_mode
        put :completion_certificate_won
        put :dictionary_views
        put :coaching_page_views
        put :calculator_views
        put :completed_modules
        put :level_activities
        get :activities, to: 'activities#for_player'
      end

      resources :activities, only: [:index] do
        get :likes
        put :likes_up
      end

      resources :videos, only: :index

      resources :dictionary_terms, only: :index

      resources :community_questions, only: [:index, :create] do
        put :close
      end
    end
  end

end
