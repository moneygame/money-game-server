require File.expand_path('../boot', __FILE__)

require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"

Bundler.require(*Rails.groups)

module MoneyGame
  class Application < Rails::Application
    config.generators do |g|
      g.test_framework :rspec
      g.factory_girl dir: 'spec/factories'
    end

    config.middleware.insert_before "ActionDispatch::Static", "Rack::Cors", :debug => true, :logger => Rails.logger do
      allow do
        origins '*'
        resource '*',
          :headers => :any,
          :methods => [:get, :post, :delete, :put, :options, :head],
          :max_age => 0
      end
    end

    config.paperclip_defaults = {
      :styles => { medium: "300x300>", thumb: "100x100>" }
    }
  end
end
