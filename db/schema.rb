# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180228160826) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "activities", force: true do |t|
    t.string   "name"
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["player_id"], name: "index_activities_on_player_id", using: :btree

  create_table "activities_players", id: false, force: true do |t|
    t.integer "player_id"
    t.integer "activity_id"
  end

  add_index "activities_players", ["activity_id"], name: "index_activities_players_on_activity_id", using: :btree
  add_index "activities_players", ["player_id"], name: "index_activities_players_on_player_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "badges", force: true do |t|
    t.string   "badge_name"
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "badges", ["player_id"], name: "index_badges_on_player_id", using: :btree

  create_table "bonus_questions", force: true do |t|
    t.string   "video_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sub_module_id"
  end

  add_index "bonus_questions", ["sub_module_id"], name: "index_bonus_questions_on_sub_module_id", using: :btree

  create_table "community_questions", force: true do |t|
    t.integer  "question_id"
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "answered_at"
    t.integer  "answered_by_id"
  end

  add_index "community_questions", ["player_id"], name: "index_community_questions_on_player_id", using: :btree
  add_index "community_questions", ["question_id"], name: "index_community_questions_on_question_id", using: :btree

  create_table "completed_modules", force: true do |t|
    t.integer  "player_id"
    t.string   "module_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "completed_modules", ["player_id"], name: "index_completed_modules_on_player_id", using: :btree

  create_table "completed_sub_modules", force: true do |t|
    t.integer "sub_module_id"
    t.integer "player_id"
  end

  add_index "completed_sub_modules", ["player_id"], name: "index_completed_sub_modules_on_player_id", using: :btree
  add_index "completed_sub_modules", ["sub_module_id"], name: "index_completed_sub_modules_on_sub_module_id", using: :btree

  create_table "dictionary_terms", force: true do |t|
    t.string   "term"
    t.text     "definition"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "game_modules", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order"
  end

  create_table "level_activities", force: true do |t|
    t.integer  "level_number"
    t.boolean  "level_completed"
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "level_activities", ["player_id"], name: "index_level_activities_on_player_id", using: :btree

  create_table "level_managers", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "player_id"
  end

  add_index "level_managers", ["player_id"], name: "index_level_managers_on_player_id", using: :btree

  create_table "module_levels", force: true do |t|
    t.integer  "game_module_id"
    t.integer  "level",            default: 1
    t.integer  "level_manager_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "module_levels", ["game_module_id"], name: "index_module_levels_on_game_module_id", using: :btree
  add_index "module_levels", ["level_manager_id"], name: "index_module_levels_on_level_manager_id", using: :btree

  create_table "options", force: true do |t|
    t.text     "text"
    t.boolean  "correct",         default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "answerable_id"
    t.string   "answerable_type"
  end

  add_index "options", ["answerable_id", "answerable_type"], name: "index_options_on_answerable_id_and_answerable_type", using: :btree

  create_table "player_calculator_views", force: true do |t|
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "player_calculator_views", ["player_id"], name: "index_player_calculator_views_on_player_id", using: :btree

  create_table "player_coaching_views", force: true do |t|
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "player_coaching_views", ["player_id"], name: "index_player_coaching_views_on_player_id", using: :btree

  create_table "player_dictionary_views", force: true do |t|
    t.integer  "player_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "player_dictionary_views", ["player_id"], name: "index_player_dictionary_views_on_player_id", using: :btree

  create_table "player_login_activities", force: true do |t|
    t.integer  "player_id"
    t.datetime "date_of_login"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "player_login_activities", ["player_id"], name: "index_player_login_activities_on_player_id", using: :btree

  create_table "players", force: true do |t|
    t.string   "email"
    t.date     "birthdate"
    t.string   "gender"
    t.string   "name"
    t.string   "facebook_id"
    t.string   "address"
    t.datetime "played_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_url"
    t.integer  "consecutive_played_days",      default: 0
    t.integer  "points",                       default: 0
    t.integer  "level",                        default: 1
    t.integer  "community_questions_answered", default: 0
    t.integer  "consecutive_levels",           default: 0
    t.string   "device_name"
    t.boolean  "educational_mode",             default: false
    t.boolean  "completion_certificate_won",   default: false
  end

  create_table "questions", force: true do |t|
    t.text     "body"
    t.text     "hint"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "explanation"
    t.string   "image_hint_file_name"
    t.string   "image_hint_content_type"
    t.integer  "image_hint_file_size"
    t.datetime "image_hint_updated_at"
    t.string   "hint_type"
    t.integer  "times_failed",            default: 0
    t.integer  "times_gained",            default: 0
    t.integer  "sub_module_id"
  end

  add_index "questions", ["sub_module_id"], name: "index_questions_on_sub_module_id", using: :btree

  create_table "reports", force: true do |t|
    t.string  "title"
    t.date    "start_date"
    t.date    "end_date"
    t.integer "number_of_users",                         default: 0
    t.integer "number_of_guest_users",                   default: 0
    t.integer "players_in_educational_mode_playing",     default: 0
    t.integer "players_in_normal_mode_playing",          default: 0
    t.integer "players_posted_questions_to_community",   default: 0
    t.integer "players_answered_questions_to_community", default: 0
    t.integer "players_won_completion_certificate",      default: 0
    t.integer "coaching_views",                          default: 0
    t.integer "dictionary_views",                        default: 0
    t.time    "average_time_spent_on_game"
    t.integer "total_downloads",                         default: 0
    t.integer "calculator_views"
  end

  create_table "sub_modules", force: true do |t|
    t.integer "game_module_id"
    t.string  "name"
    t.integer "order"
    t.integer "opening_video_id"
    t.integer "closing_video_id"
  end

  add_index "sub_modules", ["game_module_id"], name: "index_sub_modules_on_game_module_id", using: :btree

  create_table "videos", force: true do |t|
    t.string   "url"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
