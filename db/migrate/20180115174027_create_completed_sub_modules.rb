class CreateCompletedSubModules < ActiveRecord::Migration
  def change
    create_table :completed_sub_modules do |t|
      t.references :sub_module, index: true
      t.references :player, index: true
    end
  end
end
