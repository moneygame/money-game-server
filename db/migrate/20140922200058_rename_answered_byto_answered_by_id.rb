class RenameAnsweredBytoAnsweredById < ActiveRecord::Migration
  def change
    rename_column :community_questions, :answered_by, :answered_by_id
  end
end
