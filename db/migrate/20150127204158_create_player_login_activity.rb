class CreatePlayerLoginActivity < ActiveRecord::Migration
  def change
    create_table :player_login_activities do |t|
      t.references :player, index: true
      t.datetime :date_of_login
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
