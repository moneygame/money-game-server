class AddConsecutiveLevelsToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :consecutive_levels, :integer, default: 1
  end
end
