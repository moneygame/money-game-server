class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :email
      t.date :birthdate
      t.string :gender
      t.string :name
      t.string :facebook_id
      t.string :address
      t.datetime :played_at

      t.timestamps
    end
  end
end
