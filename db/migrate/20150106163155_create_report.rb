class CreateReport < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string  :title
      t.date    :start_date
      t.date    :end_date
      t.integer :number_of_users, default: 0
      t.integer :number_of_guest_users, default: 0
      t.integer :players_in_educational_mode_playing, default: 0
      t.integer :players_in_normal_mode_playing, default: 0
      t.integer :players_posted_questions_to_community, default: 0
      t.integer :players_answered_questions_to_community, default: 0
      t.integer :players_won_completion_certificate, default: 0
      t.integer :coaching_views, default: 0
      t.integer :dictionary_views, default: 0
      t.time    :average_time_spent_on_game
      t.integer :total_downloads, default: 0
    end
  end
end