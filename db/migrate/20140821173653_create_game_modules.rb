class CreateGameModules < ActiveRecord::Migration
  def change
    create_table :game_modules do |t|
      t.string :name

      t.timestamps
    end
  end
end
