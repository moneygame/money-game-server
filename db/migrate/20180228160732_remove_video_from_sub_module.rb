class RemoveVideoFromSubModule < ActiveRecord::Migration
  def change
    remove_reference :sub_modules, :video, index: true
  end
end
