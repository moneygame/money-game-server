class AddGameModuleIdToQuestions < ActiveRecord::Migration
  def change
    add_reference :questions, :game_module, index: true
  end
end
