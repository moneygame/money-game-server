class RemoveLevelNumberFromBonusQuestion < ActiveRecord::Migration
  def change
    remove_column :bonus_questions, :level_number, :integer
  end
end
