class CreatePlayerCoachingViews < ActiveRecord::Migration
  def change
    create_table :player_coaching_views do |t|
      t.references :player, index: true
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
