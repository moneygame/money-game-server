class AddConsecutivePlayedDaysToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :consecutive_played_days, :integer, default: 0
  end
end
