class ChangeColumnNameInPlayer < ActiveRecord::Migration
  def change
  	rename_column :players, :educational_mode_on, :educational_mode
  end
end
