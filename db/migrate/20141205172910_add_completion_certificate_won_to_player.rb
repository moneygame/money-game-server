class AddCompletionCertificateWonToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :completion_certificate_won, :boolean, default: false
  end
end
