class AddNameToSubModules < ActiveRecord::Migration
  def change
    add_column :sub_modules, :name, :string
  end
end
