class CreateLevelActivity < ActiveRecord::Migration
  def change
    create_table :level_activities do |t|
      t.integer :level_number
      t.boolean :level_completed
      t.references :player, index: true
      t.timestamps
    end
  end
end
