class AddPlayerReferenceToLevelManagers < ActiveRecord::Migration
  def change
    add_reference :level_managers, :player, index: true
  end
end
