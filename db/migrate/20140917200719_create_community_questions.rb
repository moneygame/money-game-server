class CreateCommunityQuestions < ActiveRecord::Migration
  def change
    create_table :community_questions do |t|
      t.references :question, index: true
      t.references :player, index: true

      t.timestamps
    end
  end
end
