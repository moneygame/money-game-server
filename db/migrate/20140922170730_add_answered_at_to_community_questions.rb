class AddAnsweredAtToCommunityQuestions < ActiveRecord::Migration
  def change
    add_column :community_questions, :answered_at, :datetime
    add_column :community_questions, :answered_by, :integer
  end
end
