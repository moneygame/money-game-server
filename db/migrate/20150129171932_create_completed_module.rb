class CreateCompletedModule < ActiveRecord::Migration
  def change
    create_table :completed_modules do |t|
      t.references :player, index: true
      t.string :module_name
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
