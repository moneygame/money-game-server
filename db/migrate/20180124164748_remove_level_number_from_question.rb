class RemoveLevelNumberFromQuestion < ActiveRecord::Migration
  def change
    remove_column :questions, :level_number, :integer
  end
end
