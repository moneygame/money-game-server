class AddOrderToSubModules < ActiveRecord::Migration
  def change
    add_column :sub_modules, :order, :integer
  end
end
