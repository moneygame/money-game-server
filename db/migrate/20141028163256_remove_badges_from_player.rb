class RemoveBadgesFromPlayer < ActiveRecord::Migration
  def change
    remove_column :players, :badges, :integer
  end
end
