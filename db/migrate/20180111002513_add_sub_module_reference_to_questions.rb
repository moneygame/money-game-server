class AddSubModuleReferenceToQuestions < ActiveRecord::Migration
  def change
    add_reference :questions, :sub_module, index: true
  end
end
