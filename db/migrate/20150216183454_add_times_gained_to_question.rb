class AddTimesGainedToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :times_gained, :integer, default: 0
  end
end
