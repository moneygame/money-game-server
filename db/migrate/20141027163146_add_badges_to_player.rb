class AddBadgesToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :badges, :integer, default: 0
  end
end
