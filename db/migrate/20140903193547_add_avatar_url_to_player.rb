class AddAvatarUrlToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :avatar_url, :string
  end
end
