class ChangeConsecutiveLevelsDefaultOnPlayer < ActiveRecord::Migration
  def change
    change_column :players, :consecutive_levels, :integer, default: 0
  end
end
