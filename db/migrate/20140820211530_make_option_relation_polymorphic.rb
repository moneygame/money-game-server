class MakeOptionRelationPolymorphic < ActiveRecord::Migration
  def change
    remove_reference :options, :question
    add_reference :options, :answerable, polymorphic: true, index: true
  end
end
