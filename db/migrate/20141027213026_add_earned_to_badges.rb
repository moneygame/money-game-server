class AddEarnedToBadges < ActiveRecord::Migration
  def change
    add_column :badges, :earned, :boolean
  end
end
