class AddLevelNumberToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :level_number, :integer
  end
end
