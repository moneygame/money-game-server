class AddCalculatorViewsToReport < ActiveRecord::Migration
  def change
    add_column :reports, :calculator_views, :integer
  end
end
