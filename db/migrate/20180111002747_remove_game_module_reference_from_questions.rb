class RemoveGameModuleReferenceFromQuestions < ActiveRecord::Migration
  def change
    remove_reference :questions, :game_module
  end
end
