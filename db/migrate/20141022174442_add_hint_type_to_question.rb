class AddHintTypeToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :hint_type, :string
  end
end
