class AddVideosToSubmodules < ActiveRecord::Migration
  def change
    add_reference :sub_modules, :opening_video, foreign_key: { to_table: :videos }
    add_reference :sub_modules, :closing_video, foreign_key: { to_table: :videos }
  end
end
