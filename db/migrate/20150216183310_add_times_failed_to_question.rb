class AddTimesFailedToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :times_failed, :integer, default: 0
  end
end
