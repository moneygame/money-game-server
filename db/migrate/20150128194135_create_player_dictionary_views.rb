class CreatePlayerDictionaryViews < ActiveRecord::Migration
  def change
    create_table :player_dictionary_views do |t|
      t.references :player, index: true
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
