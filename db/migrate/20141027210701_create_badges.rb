class CreateBadges < ActiveRecord::Migration
  def change
    create_table :badges do |t|
      t.string :badge_name
      t.references :player, index: true

      t.timestamps
    end
  end
end
