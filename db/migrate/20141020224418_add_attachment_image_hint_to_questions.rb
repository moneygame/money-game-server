class AddAttachmentImageHintToQuestions < ActiveRecord::Migration
  def self.up
    change_table :questions do |t|
      t.attachment :image_hint
    end
  end

  def self.down
    remove_attachment :questions, :image_hint
  end
end
