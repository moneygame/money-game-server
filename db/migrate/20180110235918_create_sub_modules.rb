class CreateSubModules < ActiveRecord::Migration
  def change
    create_table :sub_modules do |t|
      t.references :game_module, index: true
      t.references :video, index: true
    end
  end
end
