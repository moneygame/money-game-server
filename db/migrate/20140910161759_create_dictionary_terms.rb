class CreateDictionaryTerms < ActiveRecord::Migration
  def change
    create_table :dictionary_terms do |t|
      t.string :term
      t.text :definition

      t.timestamps
    end
  end
end
