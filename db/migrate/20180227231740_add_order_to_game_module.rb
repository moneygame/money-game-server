class AddOrderToGameModule < ActiveRecord::Migration
  def change
    add_column :game_modules, :order, :integer
  end
end
