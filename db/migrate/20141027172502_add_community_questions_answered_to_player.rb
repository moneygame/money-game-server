class AddCommunityQuestionsAnsweredToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :community_questions_answered, :integer, default: 0 
  end
end
