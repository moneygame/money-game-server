class CreateActivitiesPlayers < ActiveRecord::Migration
  def change
    create_table :activities_players, id: false do |t|
      t.references :player, index: true
      t.references :activity, index: true
    end
  end
end
