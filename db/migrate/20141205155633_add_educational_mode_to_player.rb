class AddEducationalModeToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :educational_mode_on, :boolean, default: false
  end
end
