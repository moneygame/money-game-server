class RemoveEarnedFromBadges < ActiveRecord::Migration
  def change
    remove_column :badges, :earned, :boolean
  end
end
