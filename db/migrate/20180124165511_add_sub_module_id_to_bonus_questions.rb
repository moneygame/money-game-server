class AddSubModuleIdToBonusQuestions < ActiveRecord::Migration
  def change
    add_reference :bonus_questions, :sub_module, index: true
  end
end
