class SetHintTypes < ActiveRecord::Migration
  def change
    Question.all.each do |q|
      if q.hint.present?
        q.update_attribute(:hint_type, "text")
      end
    end
  end
end
