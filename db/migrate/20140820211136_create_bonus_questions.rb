class CreateBonusQuestions < ActiveRecord::Migration
  def change
    create_table :bonus_questions do |t|
      t.string :video_url
      t.integer :level_number, index: true

      t.timestamps
    end
  end
end
