class CreateModuleLevels < ActiveRecord::Migration
  def change
    create_table :module_levels do |t|
      t.references :game_module, index: true
      t.integer :level, default: 1
      t.references :level_manager, index: true

      t.timestamps
    end
  end
end
