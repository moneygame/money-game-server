namespace :players do
  desc 'Associate unique level manager to all players'
  task associate_level_manager: :environment do

    def create_module_levels
      module_levels = []
      GameModule.all.each do |game_module|
        module_levels << ModuleLevel.new(game_module: game_module)
      end
      module_levels
    end

    Player.all.each do |player|
      @module_levels = create_module_levels
      level_manager = LevelManager.find_or_initialize_by(player: player)
      level_manager.module_levels << @module_levels
      level_manager.save!
    end
  end
end
