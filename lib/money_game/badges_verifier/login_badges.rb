require 'money_game/badges_verifier/verifier'

module MoneyGame
  module BadgesVerifier
    class LoginBadges < Verifier

      private

      def self.badge_rules
        {
          "Back-to-back learner" => Proc.new {|consecutive_days| consecutive_days == 2 },
          "Non-stop player" => Proc.new {|consecutive_days| consecutive_days == 5 }
        }
      end

      def self.value_to_compare(player)
        player.consecutive_played_days
      end

    end
  end
end
