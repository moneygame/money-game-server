require 'money_game/badges_verifier/verifier'

module MoneyGame
  module BadgesVerifier
    class CommunityBadges < Verifier

      private

      def self.badge_rules
        {
          "Community helper" => Proc.new {|community_questions_answered| community_questions_answered == 10 },
          "Community staff" => Proc.new {|community_questions_answered| community_questions_answered == 20 },
          "Community pro" => Proc.new {|community_questions_answered| community_questions_answered == 50 }
        }
      end

      def self.value_to_compare(player)
        player.community_questions_answered
      end

    end
  end
end
