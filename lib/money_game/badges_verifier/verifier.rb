module MoneyGame
  module BadgesVerifier
    class Verifier

      def self.verify_badges_for(player)
        badge_rules.each {|badge, rule| add_badge(player, badge) if rule.call(value_to_compare(player))}
      end

      private

      def self.add_badge(player, badge)
        player.badges.create(badge_name: badge)
        player.activities.create(name: "Award #{badge} badge")
      end
    end
  end
end
