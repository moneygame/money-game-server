require 'money_game/badges_verifier/verifier'

module MoneyGame
  module BadgesVerifier
    class LevelBadges < Verifier

      private

      def self.badge_rules
        {
          "3 in a row winner" => Proc.new {|consecutive_levels| consecutive_levels == 3 },
        }
      end

      def self.value_to_compare(player)
        player.consecutive_levels
      end

    end
  end
end
