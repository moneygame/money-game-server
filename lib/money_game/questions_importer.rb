module MoneyGame
  class QuestionsImporter
    OPTIONS_RANGE = (1..4)

    def self.from_csv(csv_file)
      CSV.parse(csv_file, headers: true, header_converters: :symbol) do |row|
        next if row[:module_name].blank? || row[:sub_module_name].blank?
        game_module = GameModule.where(name: row[:module_name]).first_or_create(name: row[:module_name], order: row[:module])
        sub_module = SubModule.where(name: row[:sub_module_name], game_module_id: game_module.id).first_or_create(name: row[:sub_module_name], game_module_id: game_module.id, order: row[:sub_module])
        options = options(row)
        question = {
          body: row[:body],
          hint: row[:hint],
          hint_type: row[:hint_type],
          explanation: row[:explanation],
          sub_module_id: sub_module.id,
          image_hint_file_name: row[:image_hint_file_name],
          image_hint_content_type: row[:image_hint_content_type],
          image_hint_file_size: row[:image_hint_file_size],
          image_hint_updated_at: row[:image_hint_updated_at].blank? ? nil : DateTime.parse(row[:image_hint_updated_at]),
          options: options
        }

        Question.create!(question)
      end
    end

    def self.update_questions_text_from_csv(csv_path)
      CSV.foreach(csv_path, headers: true) do |row|
        question = Question.find(row[:id].to_i)
        question.update_attribute(:body, row[:body]) if question
      end
    end

    def self.options(row)
      options = OPTIONS_RANGE.each.map { |i| row["option#{i}".to_sym] }

      options.compact.map do |option|
        Option.new(text: option, correct: option == row[:correct_option])
      end
    end

    def self.remove_obsolete_questions(updated_question_ids)
      Question.all.each do |question|
        question.destroy unless updated_question_ids.include?(question[:id])
      end
    end

    private_class_method :options, :remove_obsolete_questions
  end
end
