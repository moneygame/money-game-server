class Content
  include Prawn::View
 
  def initialize(module_questions)
    text "Money Game reporting", style: :bold, size: 30
    move_down(30)
    header_row = ["Question", "Times Completed", "Times Failed"]

    items = module_questions.map do |question|  
      [  
        question.body, 
        question.times_gained,
        question.times_failed
      ]  
    end  
    table items.unshift(header_row), :row_colors => ["FFFFFF", "DDDDDD"]

    move_down(10)  
    
  end
end