# == Schema Information
#
# Table name: player_login_activities
#
#  id            :integer          not null, primary key
#  player_id     :integer
#  date_of_login :datetime
#  created_at    :datetime
#  updated_at    :datetime
#

class PlayerLoginActivity < ActiveRecord::Base
  belongs_to :player
end
