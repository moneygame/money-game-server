# == Schema Information
#
# Table name: level_managers
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  player_id  :integer
#

class LevelManager < ActiveRecord::Base
  belongs_to :player
  has_many :module_levels
end
