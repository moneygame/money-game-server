# == Schema Information
#
# Table name: questions
#
#  id                      :integer          not null, primary key
#  body                    :text
#  hint                    :text
#  created_at              :datetime
#  updated_at              :datetime
#  explanation             :text
#  image_hint_file_name    :string(255)
#  image_hint_content_type :string(255)
#  image_hint_file_size    :integer
#  image_hint_updated_at   :datetime
#  hint_type               :string(255)
#  times_failed            :integer          default(0)
#  times_gained            :integer          default(0)
#  sub_module_id           :integer
#

class Question < ActiveRecord::Base
  belongs_to :sub_module
  has_many :options, as: :answerable, dependent: :destroy
  has_attached_file :image_hint
  has_one :report

  accepts_nested_attributes_for :options, allow_destroy: true

  validates_attachment_content_type :image_hint, content_type: /\Aimage\/.*\Z/
  validate :should_have_one_correct_option
  validates :options, length: {minimum: 2, maximum: 4,
    too_long: "has too many (maximum is 4 options)",
    too_short: "has too few (minimum is 2 options)" }

  HINT_TYPES = %w{ text image }

  def update_gained_counter
    increment(:times_gained)
  end

  def update_failed_counter
    increment(:times_failed)
  end

  private

  def should_have_one_correct_option
    errors.add(:options, "must have a correct one") if options.select {|o| o.correct }.count != 1
  end

end
