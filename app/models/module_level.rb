# == Schema Information
#
# Table name: module_levels
#
#  id               :integer          not null, primary key
#  game_module_id   :integer
#  level            :integer          default(1)
#  level_manager_id :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class ModuleLevel < ActiveRecord::Base
  validates_numericality_of :level, greater_than_or_equal_to: 1, only_integer: true
  validates :level_manager, presence: true
  validates :game_module, presence: true
  validates :game_module_id, uniqueness: { scope: :level_manager_id }

  belongs_to :level_manager
  belongs_to :game_module
end
