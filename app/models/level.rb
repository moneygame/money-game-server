class Level
  attr_reader :number
  
  QUESTIONS_PER_LEVEL = 5

  def initialize(number: 0)
    @number = number
  end

  def self.[](level_number)
    new(number: level_number)
  end

  def questions
    @questions ||= find_questions_or_random
  end

  def bonus_question
    @bonus_question ||= BonusQuestion.find_by(level_number: number)
  end

  def find_questions_or_random
    found_questions = Question.where(level_number: number).limit(QUESTIONS_PER_LEVEL)
    found_questions.concat(find_random_questions(QUESTIONS_PER_LEVEL - found_questions.count)) if found_questions.count < QUESTIONS_PER_LEVEL
    return found_questions
  end

  private

  def find_random_questions(count=5)
    Question.limit(count).order("RANDOM()")
  end

end
