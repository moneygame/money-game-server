# == Schema Information
#
# Table name: bonus_questions
#
#  id            :integer          not null, primary key
#  video_url     :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  sub_module_id :integer
#

class BonusQuestion < ActiveRecord::Base
  has_many :options, as: :answerable, dependent: :destroy
  accepts_nested_attributes_for :options
end
