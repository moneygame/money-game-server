# == Schema Information
#
# Table name: player_dictionary_views
#
#  id         :integer          not null, primary key
#  player_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class PlayerDictionaryView < ActiveRecord::Base
  belongs_to :player
end
