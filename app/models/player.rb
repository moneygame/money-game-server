# == Schema Information
#
# Table name: players
#
#  id                           :integer          not null, primary key
#  email                        :string(255)
#  birthdate                    :date
#  gender                       :string(255)
#  name                         :string(255)
#  facebook_id                  :string(255)
#  address                      :string(255)
#  played_at                    :datetime
#  created_at                   :datetime
#  updated_at                   :datetime
#  avatar_url                   :string(255)
#  consecutive_played_days      :integer          default(0)
#  points                       :integer          default(0)
#  level                        :integer          default(1)
#  community_questions_answered :integer          default(0)
#  consecutive_levels           :integer          default(0)
#  device_name                  :string(255)
#  educational_mode             :boolean          default(FALSE)
#  completion_certificate_won   :boolean          default(FALSE)
#

class Player < ActiveRecord::Base

  has_one :level_manager

  has_many :activities
  has_many :badges
  has_and_belongs_to_many :liked_activities, class_name: 'Activity'
  has_many :player_login_activities
  has_many :player_dictionary_views
  has_many :player_coaching_views
  has_many :player_calculator_views
  has_many :completed_modules
  has_many :completed_sub_modules
  has_many :level_activities

  def login(attrs = {})
    associate_level_manager if self.level_manager.nil?
    update_consecutive_played_days
    update_attributes(attrs.merge(
      consecutive_played_days: consecutive_played_days,
      played_at: Time.now
    ))
    player_login_activities.create(date_of_login: Time.now)
  end

  def level_up(level, points, consecutive_levels=0)
    activities.create(name: "Just passed Level #{level}")
    update_attributes(level: level, points: points, consecutive_levels: consecutive_levels.to_i)
  end

  def update_dictionary_views
    player_dictionary_views.create(player_id: self.id) if self.name != "guest"
  end

  def update_coaching_page_views
    player_coaching_views.create(player_id: self.id) if self.name != "guest"
  end

  def update_calculator_views
    player_calculator_views.create(player_id: self.id) if self.name != "guest"
  end

  def update_completed_modules(game_module_id)
    module_name = GameModule.find(game_module_id).name
    completed_modules.create(player_id: self.id, module_name: module_name) if self.name != "guest"
  end

  def add_level_activity(level_number, level_completed)
    level_activities.create(player_id: self.id, level_number: level_number, level_completed: level_completed) if self.name != "guest"
  end

  def update_consecutive_played_days
    return if played_today?
    self.consecutive_played_days = played_yesterday? ? consecutive_played_days + 1 : 1
  end

  private

  def associate_level_manager
    level_manager = LevelManager.new
    GameModule.all.map do |game_module|
      level_manager.module_levels << ModuleLevel.new(game_module: game_module)
    end
    self.level_manager = level_manager
    level_manager.save!
  end

  def played_today?
    played_at? Date.today
  end

  def played_yesterday?
    played_at? Date.yesterday
  end

  def played_at? date
    return false unless played_at
    played_at.to_date == date
  end

  def is_guest_user? name
    return false unless name.empty?
  end

end
