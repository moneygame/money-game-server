# == Schema Information
#
# Table name: community_questions
#
#  id             :integer          not null, primary key
#  question_id    :integer
#  player_id      :integer
#  created_at     :datetime
#  updated_at     :datetime
#  answered_at    :datetime
#  answered_by_id :integer
#

class CommunityQuestion < ActiveRecord::Base
  belongs_to :question
  belongs_to :player
  belongs_to :answered_by, class_name: Player, foreign_key: 'answered_by_id'
  has_one :report

  validates_presence_of :question, :player

  scope :open, -> { where(answered_at: nil) }
  scope :closed, -> { where.not(answered_at: nil) }

  def closed_by(player_id)
    if player_id.blank?
      errors.add_on_blank(:answered_by)
      return false
    end

    increment_community_questions_answered_by(player_id)
    player.activities.create(name: "#{Player.find(player_id).name} answered a question for #{player.name}")
    update_attributes(answered_at: DateTime.now, answered_by_id: player_id)
  end

  private

  def increment_community_questions_answered_by(player_id)
    Player.find(player_id).increment!(:community_questions_answered)
  end

end
