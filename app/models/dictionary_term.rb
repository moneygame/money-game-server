# == Schema Information
#
# Table name: dictionary_terms
#
#  id         :integer          not null, primary key
#  term       :string(255)
#  definition :text
#  created_at :datetime
#  updated_at :datetime
#

class DictionaryTerm < ActiveRecord::Base
  validates_presence_of :term
  validates_presence_of :definition
end

