# == Schema Information
#
# Table name: completed_sub_modules
#
#  id            :integer          not null, primary key
#  sub_module_id :integer
#  player_id     :integer
#

class CompletedSubModule < ActiveRecord::Base
  belongs_to :player
  belongs_to :sub_module
end
