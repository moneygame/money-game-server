# == Schema Information
#
# Table name: options
#
#  id              :integer          not null, primary key
#  text            :text
#  correct         :boolean          default(FALSE)
#  created_at      :datetime
#  updated_at      :datetime
#  answerable_id   :integer
#  answerable_type :string(255)
#

class Option < ActiveRecord::Base
  belongs_to :answerable, polymorphic: true
  default_scope -> {order :id}
end
