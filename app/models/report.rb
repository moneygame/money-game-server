# == Schema Information
#
# Table name: reports
#
#  id                                      :integer          not null, primary key
#  title                                   :string(255)
#  start_date                              :date
#  end_date                                :date
#  number_of_users                         :integer          default(0)
#  number_of_guest_users                   :integer          default(0)
#  players_in_educational_mode_playing     :integer          default(0)
#  players_in_normal_mode_playing          :integer          default(0)
#  players_posted_questions_to_community   :integer          default(0)
#  players_answered_questions_to_community :integer          default(0)
#  players_won_completion_certificate      :integer          default(0)
#  coaching_views                          :integer          default(0)
#  dictionary_views                        :integer          default(0)
#  average_time_spent_on_game              :time
#  total_downloads                         :integer          default(0)
#  calculator_views                        :integer
#

  # t.string  "title"
  # t.date    "start_date"
  # t.date    "end_date"
  # t.integer "number_of_users",                         default: 0
  # t.integer "number_of_guest_users",                   default: 0
  # t.integer "players_in_educational_mode_playing",     default: 0
  # t.integer "players_in_normal_mode_playing",          default: 0
  # t.integer "players_posted_questions_to_community",   default: 0
  # t.integer "players_answered_questions_to_community", default: 0
  # t.integer "players_won_completion_certificate",      default: 0
  # t.integer "coaching_views",                          default: 0
  # t.integer "dictionary_views",                        default: 0
  # t.time    "average_time_spent_on_game"
  # t.integer "total_downloads",                         default: 0

class Report < ActiveRecord::Base

  belongs_to :player
  belongs_to :question
  belongs_to :game_module
  belongs_to :level
  belongs_to :community_question
  belongs_to :dictionary_term

  validates :title, :start_date, :end_date, presence: true
  validate :start_date_before_end_date

  before_save :report_data

  scope :game_players, -> (start_date, end_date) { Player.where('created_at >= ? AND created_at <= ? AND name != ?', start_date, end_date.next_day, :guest) }
  scope :guest_players, -> (start_date, end_date) { Player.where('created_at >= ? AND created_at <= ? AND name = ?', start_date, end_date.next_day, :guest) }
  scope :players_won_certificate, -> { Player.where('completion_certificate_won >= ? AND name != ?', true, :guest) }
  scope :players_in_educational_mode, -> { Player.where('educational_mode = ? AND name != ?', true, :guest) }
  scope :players_in_normal_mode, -> { Player.where('educational_mode = ? AND name != ?', false, :guest) }
  scope :general_dictionary_views, -> (start_date, end_date) { PlayerDictionaryView.where('created_at >= ? AND created_at <= ?', start_date, end_date.next_day) }
  scope :general_coaching_views, -> (start_date, end_date) { PlayerCoachingView.where('created_at >= ? AND created_at <= ?', start_date, end_date.next_day) }
  scope :general_calculator_views, -> (start_date, end_date) { PlayerCalculatorView.where('created_at >= ? AND created_at <= ?', start_date, end_date.next_day) }
  scope :general_completed_modules, -> (start_date, end_date) { CompletedModule.where('created_at >= ? AND created_at <= ?', start_date, end_date.next_day) }
  scope :levels_passed, -> (start_date, end_date) { LevelActivity.where('created_at >= ? AND created_at <= ? AND level_completed = ?', start_date, end_date.next_day, true) }
  scope :levels_failed, -> (start_date, end_date) { LevelActivity.where('created_at >= ? AND created_at <= ? AND level_completed = ?', start_date, end_date.next_day, false) }

  def self.players_posted_questions(start_date, end_date)
    community_questions = CommunityQuestion.where('created_at >= ? AND created_at <= ? AND answered_at is NULL', start_date, end_date.next_day)
    players = community_questions.map(&:player_id).uniq!
    players ? players.count : 0
  end

  def self.players_answered_questions(start_date, end_date)
    community_questions = CommunityQuestion.where('created_at >= ? AND created_at <= ? AND answered_at is not NULL', start_date, end_date.next_day)
    players = community_questions.map(&:player_id).uniq!
    players ? players.count : 0
  end

  def self.player_completed_modules(start_date, end_date)
    modules = CompletedModule.where('created_at >= ? AND created_at <= ? AND answered_at is not NULL', start_date, end_date.next_day)
    module_name = modules.map(&:module_name)
  end

  def self.levels_passed_count(levels_passed)
    levels_passed.group_by(&:level_number).map do |level_number, players| [level_number, players.map(&:player_id).uniq.count] end
  end

  def self.levels_failed_count(levels_failed)
    levels_failed.group_by(&:level_number).map do |level_number, players| [level_number, players.map(&:player_id).uniq.count] end
  end

  def self.more_gained_level(passed_levels)
    passed_levels.any? ? passed_levels.sort_by(&:last).last[0] : 0
  end

  def self.more_failed_level(failed_levels)
    failed_levels.any? ? failed_levels.sort_by(&:last).last[0] : 0
  end

  def self.more_gained_question_by_module(game_module)
    game_module = GameModule.find(game_module)
    questions = game_module.questions
    more_gained_question = questions.order('times_gained DESC').first
      if more_gained_question != nil
        return more_gained_question.id
      else
        return 0
      end
  end

  def self.more_failed_question_by_module(game_module)
    game_module = GameModule.find(game_module)
    questions = game_module.questions
    more_failed_question = questions.order('times_failed DESC').first
      if more_failed_question != nil
        return more_failed_question.id
      else
        return 0
      end
  end

  private

  def start_date_before_end_date
    unless start_date.nil? || end_date.nil?
      errors.add(:start_date, "Start date cannot be greather than end date") if end_date < start_date
    end
  end

  def report_data
    self.number_of_users = Report.game_players(self.start_date, self.end_date).count
    self.number_of_guest_users = Report.guest_players(self.start_date, self.end_date).count
    self.players_in_educational_mode_playing = Report.players_in_educational_mode.count
    self.players_in_normal_mode_playing = Report.players_in_normal_mode.count
    self.players_won_completion_certificate = Report.players_won_certificate.count
    self.players_posted_questions_to_community = Report.players_posted_questions(self.start_date, self.end_date)
    self.players_answered_questions_to_community = Report.players_answered_questions(self.start_date, self.end_date)
    self.dictionary_views = Report.general_dictionary_views(self.start_date, self.end_date).count
    self.coaching_views = Report.general_coaching_views(self.start_date, self.end_date).count
    self.calculator_views = Report.general_calculator_views(self.start_date, self.end_date).count
  end

end
