# == Schema Information
#
# Table name: player_coaching_views
#
#  id         :integer          not null, primary key
#  player_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class PlayerCoachingView < ActiveRecord::Base
  belongs_to :player
end
