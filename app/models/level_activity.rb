# == Schema Information
#
# Table name: level_activities
#
#  id              :integer          not null, primary key
#  level_number    :integer
#  level_completed :boolean
#  player_id       :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class LevelActivity < ActiveRecord::Base
  belongs_to :player
end
