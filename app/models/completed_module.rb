# == Schema Information
#
# Table name: completed_modules
#
#  id          :integer          not null, primary key
#  player_id   :integer
#  module_name :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class CompletedModule < ActiveRecord::Base
  belongs_to :player
end
