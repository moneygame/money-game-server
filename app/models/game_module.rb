# == Schema Information
#
# Table name: game_modules
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  order      :integer
#

class GameModule < ActiveRecord::Base
  has_many :module_levels
  has_many :sub_modules
  has_one :report
end
