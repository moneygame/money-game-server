# == Schema Information
#
# Table name: badges
#
#  id         :integer          not null, primary key
#  badge_name :string(255)
#  player_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class Badge < ActiveRecord::Base
	belongs_to :player
end
