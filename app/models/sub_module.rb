# == Schema Information
#
# Table name: sub_modules
#
#  id               :integer          not null, primary key
#  game_module_id   :integer
#  name             :string(255)
#  order            :integer
#  opening_video_id :integer
#  closing_video_id :integer
#

class SubModule < ActiveRecord::Base
  belongs_to :game_module
  belongs_to :opening_video, class_name: 'Video'
  belongs_to :closing_video, class_name: 'Video'
  has_many :questions
end
