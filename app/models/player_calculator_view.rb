# == Schema Information
#
# Table name: player_calculator_views
#
#  id         :integer          not null, primary key
#  player_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class PlayerCalculatorView < ActiveRecord::Base
  belongs_to :player
end
