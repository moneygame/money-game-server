class ActivitySerializer < ActiveModel::Serializer
  attributes :id, :name, :likes_count, :player, :like_ids

  def likes_count
    object.likes.count
  end

end
