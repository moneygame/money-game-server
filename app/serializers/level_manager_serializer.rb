class LevelManagerSerializer < ActiveModel::Serializer
  attributes :id, :player_id, :created_at, :updated_at
  has_many :module_levels
end
