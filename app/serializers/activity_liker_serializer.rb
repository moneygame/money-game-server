class ActivityLikerSerializer < ActiveModel::Serializer
  attributes :id, :name, :level, :avatar_url
end
