class GameModuleSerializer < ActiveModel::Serializer
  attributes :id, :name, :order
  has_many :sub_modules

  def sub_modules
    object.sub_modules.order(:order)
  end

  def serializable_hash
    data = super
    data.delete(:sub_modules) if options[:template] == "index"
    data
  end
end
