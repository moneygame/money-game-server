class DictionaryTermSerializer < ActiveModel::Serializer
  attributes :term, :definition
end
