class OptionSerializer < ActiveModel::Serializer
  attributes :id, :text, :correct
end
