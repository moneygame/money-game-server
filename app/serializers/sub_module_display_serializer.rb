class SubModuleDisplaySerializer < ActiveModel::Serializer
  attributes :id, :name
end