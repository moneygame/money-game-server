class QuestionSerializer < ActiveModel::Serializer
  attributes :id, :body, :hint, :explanation, :hint_type
  has_many :question_options

  def question_options
    object.options
  end

  def hint
    if object.hint_type == "text"
      object.hint
    elsif object.hint_type == "image"
      object.image_hint.url(:medium)
    else
      nil
    end
  end
end
