class ModuleLevelSerializer < ActiveModel::Serializer
  attributes :id, :game_module_id, :level, :created_at, :updated_at
end
