class CompletedSubModuleSerializer < ActiveModel::Serializer
  attributes :id

  has_one :sub_module, serializer: SubModuleDisplaySerializer
end