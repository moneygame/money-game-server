class SubModuleSerializer < ActiveModel::Serializer
  attributes :id, :game_module_id, :name, :order
  has_one :opening_video
  has_one :closing_video
  has_many :questions

  def serializable_hash
    data = super
    data.delete(:questions) if options[:template] == "index"
    data
  end
end
