class PlayerSerializer < ActiveModel::Serializer
  attributes :id, :facebook_id, :email, :gender, :birthdate, :address, :name, :played_at, :avatar_url, :points, :badges_count

  has_many :completed_sub_modules

  def badges_count
    object.badges.count
  end
end
