class CommunityQuestionSerializer < ActiveModel::Serializer
  attributes :id, :question_id

  def serializable_hash
    data = super
    data[:player] = {
      id: object.player.try(:id),
      name: object.player.try(:name)
    }
    data
  end
end
