class BonusQuestionSerializer < ActiveModel::Serializer
  attributes :id, :video_url
  has_many :question_options

  def question_options
    object.options
  end
end
