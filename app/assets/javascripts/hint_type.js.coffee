$ ->
  $textHint = $("#question_hint_input")
  $imageHint = $("#question_image_hint_input")
  $hintTypeRadios =  $('#question_hint_type_input input')

  displayHintTypeInput = (hintType)->
    if hintType  == "text"
      $textHint.show()
      $imageHint.hide()
    else if hintType == "image"
      $textHint.hide()
      $imageHint.show()
    else
      $.merge($textHint,$imageHint).hide()


  displayHintTypeInput($hintTypeRadios.filter(':checked').val())

  $hintTypeRadios.change ->
    displayHintTypeInput($(this).val())
