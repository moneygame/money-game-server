ActiveAdmin.register DictionaryTerm do
  permit_params :term, :definition
end
