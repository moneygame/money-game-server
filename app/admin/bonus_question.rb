ActiveAdmin.register BonusQuestion do
  permit_params :video_url, :sub_module_id, options_attributes: [:question_id, :text, :correct, :_destroy]

  form do |f|
    f.inputs do
      f.input :sub_module_id
      f.input :video_url
    end

    f.inputs do
      f.has_many :options, allow_destroy: true, heading: 'Options', new_record: true do |of|
        of.input :text, as: :string
        of.input :correct
      end
    end

    f.actions
  end
end
