ActiveAdmin.register CommunityQuestion do
  permit_params :player_id, :question_id

  scope :open
  scope :closed
end
