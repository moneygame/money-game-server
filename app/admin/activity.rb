ActiveAdmin.register Activity do
   permit_params :name, :player_id, like_ids: []

   form do |f|
     f.semantic_errors(*f.object.errors.keys)

     f.inputs do
       f.input :name
       f.input :player, as: :select
       f.input :likes, as: :check_boxes
     end

     f.actions
   end
end
