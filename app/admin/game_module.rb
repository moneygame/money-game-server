require 'content_pdf'

ActiveAdmin.register GameModule do
  permit_params :name, :order

  member_action :questions do
    @game_module = GameModule.find(params[:id])
    @questions = @game_module.questions
    @report = Report.find(params[:report_id])
  end

  action_item only: :questions do
    link_to "Back to modules", modules_admin_report_path(report)
  end

  action_item only: :questions do
    link_to "Download", module_questions_download_path(game_module)
  end

end
