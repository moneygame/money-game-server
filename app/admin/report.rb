ActiveAdmin.register Report do
  permit_params :start_date, :end_date, :title, :id
  actions :all, except: :edit
  filter :title
  
  controller do
    before_filter { @page_title = "Reporting" }
  end

  member_action :players do
    @report = Report.find(params[:id])
    @players = Report.game_players(@report.start_date, @report.end_date)
  end

  member_action :levels do
    @report = Report.find(params[:id])
    @levels_passed = Report.levels_passed(@report.start_date, @report.end_date).order(:level_number)
    @passed_levels_counter = Report.levels_passed_count(@levels_passed)
    @more_gained_level = Report.more_gained_level(@passed_levels_counter)
    @levels_failed = Report.levels_failed(@report.start_date, @report.end_date).order(:level_number)
    @failed_levels_counter = Report.levels_failed_count(@levels_failed)
    @more_failed_level = Report.more_failed_level(@failed_levels_counter)
  end

  member_action :modules do
    @report = Report.find(params[:id])
    @modules = GameModule.all
  end

  action_item only: [:players, :levels, :modules] do
    link_to "Back to report details", admin_report_path(report)
  end

  action_item only: [:players, :levels, :modules] do
    link_to "Download"
  end

  index do
    selectable_column
    column :title
    column :start_date
    column :end_date
      actions
  end

  show do
    attributes_table do
      row :title
      row :start_date
      row :end_date
      row :number_of_users do |report|
        link_to(report.number_of_users, players_admin_report_path(report))
      end
      row :number_of_guest_users
      row :players_in_educational_mode_playing
      row :players_in_normal_mode_playing
      row :players_posted_questions_to_community
      row :players_answered_questions_to_community
      row :players_won_completion_certificate
      row :coaching_views
      row :dictionary_views
      row :calculator_views
      row "Levels information" do
        link_to("Levels activity", levels_admin_report_path(report))
      end
      row "Modules information" do
        link_to("Modules activity", modules_admin_report_path(report))
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :start_date, as: :datepicker, datepicker_options: { min_date: "2014-6-1", max_date: :actual_date }
      f.input :end_date,   as: :datepicker, datepicker_options: { min_date: "2014-6-1", max_date: :actual_date }
      f.actions
    end
  end

end
