ActiveAdmin.register SubModule do

  permit_params :game_module_id, :opening_video_id, :closing_video_id, :name, :order

end
