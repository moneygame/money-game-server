ActiveAdmin.register Player do
  permit_params :email, :birthdate, :gender, :name, :address, :played_at, :avatar_url, :consecutive_played_days, :community_questions_answered

  member_action :player_login_dates do
    @player = Player.find(params[:id])
    @dates_of_login = @player.player_login_activities
    @report = Report.find(params[:report_id])
  end

  member_action :player_completed_modules do
    @player = Player.find(params[:id])
    @completed_modules = @player.completed_modules
    @report = Report.find(params[:report_id])
  end

  action_item only: [:player_login_dates, :player_completed_modules] do
    link_to "Back to players", players_admin_report_path(report)
  end

  action_item only: [:player_login_dates] do
    link_to "Download", player_login_dates_admin_player_path(id: player, report_id: report)
  end

  action_item only: [:player_completed_modules] do
    link_to "Download", player_completed_modules_admin_player_path(id: player, report_id: report)
  end

end
