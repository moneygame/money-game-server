require 'csv'
require 'money_game/questions_importer'

ActiveAdmin.register Question do
  permit_params :body, :explanation, :hint, :image_hint, :hint_type,
                :sub_module_id, :times_gained, :times_failed,
                options_attributes: [:id, :text, :correct, :_destroy]

  action_item only: :index do
    link_to 'Import Questions', action: 'upload_csv'
  end

  collection_action :upload_csv do; end

  collection_action :import_csv, method: :post do
    csv_file = params[:dump][:file].read
    MoneyGame::QuestionsImporter.from_csv(csv_file)

    redirect_to action: :index, notice: 'Questions imported successfully!'
  end

  csv do
    column :id
    column :body
    column :hint
    column :created_at
    column :updated_at
    column :sub_module_id
    column :explanation
    column :image_hint_file_name
    column :image_hint_content_type
    column :image_hint_file_size
    column :image_hint_updated_at
    column :hint_type
    column :times_failed
    column :times_gained
    column(:correct_option) { |question| question.options.find_by(correct: true).try(:text) }
    column(:option1) { |question| question.options[0].try(:text) }
    column(:option2) { |question| question.options[1].try(:text) }
    column(:option3) { |question| question.options[2].try(:text) }
    column(:option4) { |question| question.options[3].try(:text) }
  end

  form do |f|
    f.semantic_errors(*f.object.errors.keys)

    f.inputs do
      f.input :sub_module, as: :select
      f.input :body, input_html: {rows: 6}
      f.input :explanation, input_html: {rows: 3}
      f.input :hint_type, as: :radio, collection: Question::HINT_TYPES.map {|h| ["#{h.capitalize} Hint", h]}
      f.input :hint, input_html: {rows: 3}
      f.input :image_hint, as: :file, hint: f.object.image_hint.present? ? f.template.image_tag(f.object.image_hint.url(:thumb)) : nil
      f.input :times_gained
      f.input :times_failed
    end

    f.inputs do
      f.has_many :options, heading: 'Options', new_record: true do |of|
        of.input :text, as: :string
        of.input :correct
        if !of.object.nil?
          of.input :_destroy, :as => :boolean, :label => "Destroy?"
        end
      end
    end

    f.actions
  end
end
