require 'content_pdf'

class PageController < ApplicationController

  def module_questions_download
    @module = GameModule.find(params[:game_module_id])
  	send_data Content.new(@module.questions).render, filename: 'report.pdf', type: 'application/pdf'
  end

end