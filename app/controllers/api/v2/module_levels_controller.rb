class API::V2::ModuleLevelsController < ApplicationController
  before_action :find_module_level, only: [:level_up]

  def level_up
    if @module_level
      @module_level.update_attribute(:level, @module_level.level + 1)
      render json: @module_level
    else
      render json: { "error" => "This module level doesn't exists" }
    end
  end

  private

  def find_module_level
    @module_level = ModuleLevel.includes(:level_manager)
      .find_by(game_module_id: params[:game_module_id],
               level_managers: { player_id: params[:player_id] }
              )
  end
end
