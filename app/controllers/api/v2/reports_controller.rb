class API::V2::ReportsController < ApplicationController

  #before_filter { @page_title = "Reporting" }
  #layout 'active_admin' 

  def new
    @report = Report.new(report_params)
  end

  def index 
    Report.all 
  end

  private

  def report_params
    params.require(:report).permit(:start_date, :end_date)
  end

end
