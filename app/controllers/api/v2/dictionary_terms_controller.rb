class API::V2::DictionaryTermsController < ApplicationController
  def index
    render json: DictionaryTerm.all
  end
end
