class API::V2::GameModulesController < ApplicationController

  def index
    render json: GameModule.all
  end

  def show
    render json: GameModule.find(params[:id])
  end

  def questions
    game_module = GameModule.find(params[:id])
    x1_range = ((params[:module_level].to_i - 1) * 5)
    x2_range = ((params[:module_level].to_i * 5) - 1)
    questions = game_module.questions[x1_range..x2_range]
    render json: { "questions" => questions.as_json({include: :options}) }
  end
end
