class API::V2::VideosController < ApplicationController

  def index
    render json: Video.all
  end
end
