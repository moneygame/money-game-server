class API::V2::BonusQuestionsController < ApplicationController

  def show
    bonus_question = Level[params[:level_number]].bonus_question
    render json: bonus_question
  end

end
