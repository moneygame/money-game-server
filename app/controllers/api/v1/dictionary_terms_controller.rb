class API::V1::DictionaryTermsController < ApplicationController
  def index
    render json: DictionaryTerm.all
  end
end
