require 'money_game/badges_verifier/level_badges'
require 'money_game/badges_verifier/login_badges'

class API::V1::PlayersController < ApplicationController
  include MoneyGame::BadgesVerifier

  def show
    player = Player.find(params[:id]) rescue nil
    render json: player
  end

  def login
    player = Player.find_by(facebook_id: login_attributes[:facebook_id]) || Player.new
    if player.login(login_attributes)
      LoginBadges.verify_badges_for(player)
      render json: player
    else
      render json: {error: "An error occurred when trying to log in the player"}, status: :unprocessable_entity
    end
  end

  def level_up
    player = Player.find(params[:player_id])
    if player.level_up(level_up_params[:level], level_up_params[:points], level_up_params[:consecutive_levels])
      LevelBadges.verify_badges_for(player)
      render json: player
    else
      render json: {error: "An error occurred when trying to level up the player"}, status: :unprocessable_entity
    end
  end

  def playing_educational_mode
    player = Player.find(params[:player_id])
    player.educational_mode = educational_mode_params[:educational_mode]
    if player.save
      render json: player
    else 
      render json: {error: "An error occurred when trying to set educational mode state for player"}, status: :unprocessable_entity
    end
  end

  def completion_certificate_won
    player = Player.find(params[:player_id])
    player.completion_certificate_won = completion_certificate_won_params[:completion_certificate_won]
    if player.save
      render json: player
    else 
      render json: {error: "An error occurred when trying to set completion certificate for player"}, status: :unprocessable_entity
    end
  end  

  def dictionary_views
    player = Player.find(params[:player_id])
    if player.update_dictionary_views
      render json: player
    else
      render json: {error: "An error occurred when trying to update dictionary_views to player"}, status: :unprocessable_entity
    end
  end

  def coaching_page_views
    player = Player.find(params[:player_id])
    if player.update_coaching_page_views
      render json: player
    else
      render json: {error: "An error occurred when trying to update coaching_page_views to player"}, status: :unprocessable_entity
    end
  end

  def calculator_views
    player = Player.find(params[:player_id])
    if player.update_calculator_views
      render json: player
    else
      render json: {error: "An error occurred when trying to update calculator_views to player"}, status: :unprocessable_entity
    end
  end

  def completed_modules
    player = Player.find(params[:player_id])
    if player.update_completed_modules(completed_modules_params[:module])
      render json: player
    else
      render json: {error: "An error occurred when trying to update completed modules to player"}, status: :unprocessable_entity
    end
  end

  def level_activities
    player = Player.find(params[:player_id])
    if player.add_level_activity(level_activity_params[:level], level_activity_params[:level_completed])
      render json: player
    else
      render json: {error: "An error occurred when trying to update level activities to player"}, status: :unprocessable_entity
    end
  end

  private

  def login_attributes
    params.require(:player).permit(:facebook_id, :email, :gender, :birthdate, :address, :name, :avatar_url, :device_name)
  end

  def level_up_params
    params.require(:player).permit(:level, :points, :consecutive_levels)
  end

  def educational_mode_params
    params.require(:player).permit(:educational_mode)
  end

  def completion_certificate_won_params
    params.require(:player).permit(:completion_certificate_won)
  end

  def completed_modules_params
    params.require(:player).permit(:module)
  end

  def level_activity_params
    params.require(:player).permit(:level, :level_completed)
  end

end
