class API::V1::VideosController < ApplicationController

  def index
    render json: Video.all
  end
end
