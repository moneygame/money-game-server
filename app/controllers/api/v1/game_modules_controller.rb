class API::V1::GameModulesController < ApplicationController

  def index
    render json: GameModule.all
  end

  def show
    render json: GameModule.find(params[:id])
  end
end
