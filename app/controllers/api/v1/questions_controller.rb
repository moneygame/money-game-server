class API::V1::QuestionsController < ApplicationController

  def index
    level = Level[params[:level_number]]
    render json: level.questions
  end

  def show
    question = Question.find(params[:id])
    render json: question
  end

  def max_level
  	max_level = Question.all.maximum(:level_number)
  	render json: {max_level: max_level}
  end

  def ninety_percent_questions
    questions_number = Question.all.count
    ninety_percent_questions = (questions_number * 0.9).floor
    render json: {ninety_percent_questions: ninety_percent_questions}
  end

  def update_gained_question_counter
    question = Question.find(params[:question_id])
    if question.update_gained_counter
      render json: question
    else
      render json: {error: "An error occurred when trying to update gained question counter"}, status: :unprocessable_entity
    end
  end

  def update_failed_question_counter
    question = Question.find(params[:question_id])
    if question.update_failed_counter
      render json: question
    else
      render json: {error: "An error occurred when trying to update failed question counter"}, status: :unprocessable_entity
    end
  end

end
