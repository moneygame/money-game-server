class API::V1::ActivitiesController < ApplicationController

  def index
    render json: Activity.order(created_at: :desc).limit(30)
  end

  def for_player
    player = Player.find(params[:player_id])
    render json: player.activities.order(updated_at: :desc).limit(30)
  end

  def likes
    activity = Activity.find(params[:activity_id])
    render json: activity.likes, each_serializer: ActivityLikerSerializer, root: "activity_likers"
  end

  def likes_up
    player_id = params[:player][:id].to_i
    activity = Activity.find(params[:activity_id])
    unless activity.like_ids.include? player_id
      activity.like_ids = activity.like_ids.push(player_id)
    end
    render json: activity
  end
end
