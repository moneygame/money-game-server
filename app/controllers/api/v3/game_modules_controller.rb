class API::V3::GameModulesController < ApplicationController

  def index
    render json: GameModule.order(:order)
  end

  def show
    render json: GameModule.find(params[:id])
  end
end