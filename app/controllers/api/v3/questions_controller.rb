class API::V3::QuestionsController < ApplicationController

  def show
    question = Question.find(params[:id])
    render json: question
  end

  def ninety_percent_questions
    questions_number = Question.all.count
    ninety_percent_questions = (questions_number * 0.9).floor
    render json: {ninety_percent_questions: ninety_percent_questions}
  end

  def update_gained_question_counter
    question = Question.find(params[:question_id])
    if question.update_gained_counter
      render json: question
    else
      render json: {error: "An error occurred when trying to update gained question counter"}, status: :unprocessable_entity
    end
  end

  def update_failed_question_counter
    question = Question.find(params[:question_id])
    if question.update_failed_counter
      render json: question
    else
      render json: {error: "An error occurred when trying to update failed question counter"}, status: :unprocessable_entity
    end
  end

end
