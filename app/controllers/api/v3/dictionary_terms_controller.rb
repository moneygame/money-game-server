class API::V3::DictionaryTermsController < ApplicationController
  def index
    render json: DictionaryTerm.all
  end
end
