class API::V3::VideosController < ApplicationController

  def index
    render json: Video.all
  end

  def show
    video = Video.find(params[:id]) rescue nil
    render json: video
  end
end
