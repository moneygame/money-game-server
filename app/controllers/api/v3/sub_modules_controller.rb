class API::V3::SubModulesController < ApplicationController

  def index
    render json: SubModule.all
  end

  def show
    sub_module = SubModule.find(params[:id]) rescue nil
    render json: sub_module
  end

  def questions
    sub_module = SubModule.find(params[:sub_module_id]) rescue nil
    if sub_module
      questions = sub_module.questions
      render json: { "questions" => questions.as_json({include: :options}) }
    else
      render json: { error: "Sub module not found" }
    end
  end

  def complete
    sub_module = SubModule.find(params[:sub_module_id])
    completed_sub_module = CompletedSubModule.where(sub_module_id: sub_module.id, player_id: params[:player_id]).first
    unless CompletedSubModule.where(sub_module_id: sub_module.id, player_id: params[:player_id]).first
      completed_sub_module = CompletedSubModule.new(sub_module_id: sub_module.id, player_id: params[:player_id])
      if completed_sub_module.save
        render json: completed_sub_module
      else
        render json: {errors: completed_sub_module.errors.full_messages }, status: :unprocessable_entity
      end
    else
      render json: completed_sub_module
    end
  end
end