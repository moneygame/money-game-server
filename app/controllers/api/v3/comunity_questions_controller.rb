require 'money_game/badges_verifier/community_badges'

class API::V3::CommunityQuestionsController < ApplicationController
  include MoneyGame::BadgesVerifier

  def index
    render json: CommunityQuestion.open.order(created_at: :desc)
  end

  def create
    community_question = CommunityQuestion.new(community_question_params)

    if community_question.save
      render json: community_question
    else
      render json: {errors: community_question.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def close
    community_question = CommunityQuestion.find(params[:community_question_id])

    if community_question.closed_by(params[:answered_by].to_i)
      CommunityBadges.verify_badges_for(Player.find(params[:answered_by].to_i))
      render json: community_question
    else
      render json: {errors: community_question.errors.full_messages}, status: :unprocessable_entity
    end
  end

  private

  def community_question_params
    params.require(:community_question).permit(:player_id, :question_id)
  end
end
